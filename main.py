import os
import re
import sys
import cv2
import time
import shutil
import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from modules import helperFunctions as hf


# TO DO:
# Option for applying the script to a single image instead of to a folder?
# Error message if the regex returns no results? 

#--------------------------------------------------------------------------

# ARGUMENT PARSING

# NOT WORKING. IT NEVER HAS. FUCK.


argv = sys.argv[1:]

print(argv)

parser = argparse.ArgumentParser()

## Camera

# Mark this as required in the future, leave it like this for testing.
parser.add_argument("--camera", dest = "camera", nargs = 1, required = False,
        help = "Name of the camera to be used (VIS or NIR)")

## Image source

parser.add_argument("--foldername", dest = "foldername", nargs = 2, required = False,
        default = "Images", # ???
        help = "Folder containing the original images")

args = parser.parse_args(argv)
print(args)

#--------------------------------------------------------------------------

# USER INPUT

## Not working, it asks you again

try:
    camera = args.camera[0]
except:
    camera = input("Select camera (VIS or NIR): ")

# if written wrong then 
if camera != "VIS" and camera != "NIR":
    print(f"'{camera}' is not correct")
    sys.exit()

#--------------------------------------------------------------------------

# IMPORTING  DATASETS

## VIS

# Import throughput data
vis = pd.read_csv("data/vis_throughput.csv") 

# Column names are detected automatically, but row names are not.
# The first column is called "0" in the original file. Could use
# a better name, but I don't want to touch that.
vis = vis.set_index("0")

# Import gap_vs_wavelength dataset.

gaps = pd.read_csv("data/gap_vs_wavelength.csv")
# Rename column names, as they contain spaces.
gaps.columns = ["Gap", "WL8", "WL7", "WL6", "WL5", "WL4", "WL3", "WL2", "WL1"]
# Using first column as row names
gaps = gaps.set_index("Gap")

# Integrate the values over columns in order to get the (?) total transmission.
integratedVIS = vis.sum(axis = 0)
integratedVIS = integratedVIS / 551   # (?) Normalization. Is it correct?

# Import quantum efficiency file
quantumEff = pd.read_csv("data/quantum_eff_VIS.csv")
# Using first column as row names
#quantumEff = quantumEff.set_index("WL[nm]")

## NIR

##### Missing!


#--------------------------------------------------------------------------

# IMAGE FILTERING


foldername = "Images"
folderPath = os.getcwd() + "/" + foldername

# NOT WORKING NOW???
newFolderPath = folderPath + f"_{camera}_filtered"

# Can't understand why the next two don't work

#newFolderPath = folderPath + f"_{camera}_filtered_" + time.strftime("%d%m%Y-%H%M%S")
#newFolderPath = folderPath + f"_{camera}_filtered_{time.strftime('%d%m%Y-%H%M%S')}" 

# This confirmation request may be an inconvenience for automation.

"""
if not os.path.exists(newFolderPath):
    os.makedirs(newFolderPath)
    print("Created folder '" + newFolderPath + "'")

else:
    question = input("Folder already exists, results may be overwritten. Continue? (y/n)")

    if question.lower() != "y":

        print("Closing.")
        sys.exit()
"""

os.chdir(folderPath)    # Moving back to the folder with the original images.

filenames = [f for f in os.listdir(folderPath) if os.path.isfile(os.path.join(folderPath, f))]
filenames = sorted(filenames)

p = re.compile("\d+.?\d+nm")    # Regex that finds the #.#nm pattern

## VIS

if camera == "VIS":

    multiplier = 2000  # Multiplier so you can distinguish something in the result.
    #multiplier = 2000  # Multiplier so you can distinguish something in the result.
    print(f"Multiplier: x{multiplier}")
    i = 1

    for filename in filenames:

        print(f"Filtering image number {i}")

        # Extract wavelength
        wavelength = float(p.match(filename).group()[:-2])
        # Find correspoding gap
        gap = hf.wlToGap(wavelength, gaps)
        # RGB throughput
        RGB = hf.RGBThroughput(gap, integratedVIS)
     
        RGB = tuple(i * multiplier for i in RGB)

        # Quantum efficiency
        quantumEfficiencies = hf.wlToQuantumEfficiencies(wavelength, quantumEff)
        #QERed, QEGreen, QEBlue = hf.wlToQuantumEfficiencies(wavelength, quantumEff)

        img = cv2.imread(filename, 1)
        
        print(RGB)
        print(quantumEfficiencies)

        
        # Throughput is multiplied in this function. Quantum efficiency should, too, right?
        # First: new function that (interpolates) looks for the values for the desired wavelength.
        # Next: multiply by the quantum effs, and correct the original file!!!!
        # Missing only the original file correction. Harder than it looks.

        R, R_filtered, G, G1_filtered, G2_filtered, B, B_filtered = \
                hf.filterAndMosaic(img, RGB, quantumEfficiencies)

        # Saving result in new folder
        filename = filename.removesuffix(".png")

        # Red
        #cv2.imwrite(newFolderPath + "/" + filename + "_original_R.png", R) 
        cv2.imwrite(newFolderPath + "/" + filename + "_filtered_R.png", R_filtered) 

        # Green1
        #cv2.imwrite(newFolderPath + "/" + filename + "_original_G.png", G) 
        cv2.imwrite(newFolderPath + "/" + filename + "_filtered_G1.png", G1_filtered) 

        # Green2
        cv2.imwrite(newFolderPath + "/" + filename + "_filtered_G2.png", G2_filtered) 

        # Blue
        #cv2.imwrite(newFolderPath + "/" + filename + "_original_B.png", B) 
        cv2.imwrite(newFolderPath + "/" + filename + "_filtered_B.png", B_filtered) 

        # Grayscale!
        # This is the original image, right??
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        cv2.imwrite(newFolderPath + "/" + filename + "_original.png", img) 
        #cv2.imwrite(newFolderPath + "/" + filename + "_filtered_gray.png", img) 

        i += 1


## NIR

elif camera == "NIR":

    #boxcar.plot(x = "wavelength", y = "height")
    #plt.show()

    i = 1

    for filename in filenames:

        print(f"Filtering image number {i}")

        # Extract wavelength
        wavelength = float(p.match(filename).group()[:-2])

        # Boxcar function centered around that wavelength
        boxcar = hf.boxcarGenerator(400, 800, focus = wavelength, width = 20, height = .2)
        # The real values are between 1000 - 1600!
        #boxcar = boxcarGenerator(1000, 1600, focus = wavelength, width = 20, height = .2)

        integratedNIR = boxcar["height"].sum()

        # Normalization
        nonZeroValues = np.count_nonzero(boxcar["height"])
        integratedNIR = integratedNIR / nonZeroValues

        img = cv2.imread(filename, 1)
        np.multiply(img, integratedNIR, out = img, casting='unsafe')    # not doing anything
        
        # Saving result in new folder
        filename = filename.removesuffix(".png")
        cv2.imwrite(newFolderPath + "/" + filename + "_filtered.png", img) 

        i += 1

else:

    print("Incorrect camera selected")

print("Results saved in '" + newFolderPath + "'")
