import cv2
import numpy as np
import pandas as pd

def wlToGap(wl, gaps):
    """
    Returns the equivalent airgap for a certain wavelength for VIS.
    Wavelength in nm, please.
    """

    row = 0 # Is there a smarter solution for this?
    
    for item in gaps["WL1"]:

        if int(item) > wl: # int() truncates the value.

            gap = gaps.index[row - 1]
            break

        row += 1

    return gap

def wlToQuantumEfficiencies(wl, quantumEff):
    """
    Returns Quantum efficiency for R, G, B for a given wavelength.
    """

    row = 0

    for item in quantumEff["WL[nm]"]:

        if int(item) > wl: # int() truncates the value.

            QERed = quantumEff.iloc[row - 1]["QE_Red"]
            QEBlue = quantumEff.iloc[row - 1]["QE_Blue"]
            QEGreen = quantumEff.iloc[row - 1]["QE_Green"]
            break

        row += 1
    
    # Reordered the return to RGB, I don't want any confusions.
    return (QERed, QEGreen, QEBlue)


def RGBThroughput(gap, integratedVIS):
    """
    Returns the R, G and B throughput (?) for a certain airgap, as a tuple.
    Gap in nm.
    """

    # Note: columns get renamed to | gap | gap.1 | gap.2 | when imported with Pandas.
    RThroughput = float(integratedVIS[str(gap)])
    GThroughput = float(integratedVIS[str(gap + .1)])
    BThroughput = float(integratedVIS[str(gap + .2)])

    return(RThroughput, GThroughput, BThroughput)


def filterAndMosaic(img, RGBThroughput, quantumEfficiencies):
#def filterAndMosaic(img, RGBThroughput):
    """
    filterAndMerge is not actually what they want, this function
    is similar but not quite the same.
    """

    # Splitting into channels (CV2 uses BGR)
    (B, G, R) = cv2.split(img)

    # Channel "degradation"
    np.multiply(R, RGBThroughput[0], out=R, casting="unsafe")
    np.multiply(G, RGBThroughput[1], out=G, casting="unsafe")
    np.multiply(B, RGBThroughput[2], out=B, casting="unsafe")

    # Also with quantum efficiencies:
    np.multiply(R, quantumEfficiencies[0], out=R, casting="unsafe")
    np.multiply(G, quantumEfficiencies[1], out=G, casting="unsafe")
    np.multiply(B, quantumEfficiencies[2], out=B, casting="unsafe")


    # Pattern:
    # R G
    # G B
    # Future work: allow choice of pattern

    # Red
    # Keep odd rows and columns
    R_oddRows = np.delete(R, range(1, R.shape[0], 2), axis=0)
    R_oddRows_oddColumns = np.delete(R_oddRows, range(1, R_oddRows.shape[1], 2), axis=1)
    R_filtered = R_oddRows_oddColumns

    # Green 1
    # Keep odd rows and even columns
    G_oddRows = np.delete(G, range(1, G.shape[0], 2), axis=0)
    G_oddRows_evenColumns = np.delete(G_oddRows, range(0, G_oddRows.shape[1], 2), axis=1)
    G1_filtered = G_oddRows_evenColumns

    # Green 2
    # Keep even rows and odd columns
    G_evenRows = np.delete(G, range(0, G.shape[0], 2), axis=0)
    G_evenRows_oddColumns = np.delete(G_evenRows, range(1, G_evenRows.shape[1], 2), axis=1)
    G2_filtered = G_evenRows_oddColumns

    # Blue
    # Keep even rows and columns
    B_evenRows = np.delete(B, range(0, B.shape[0], 2), axis=0) 
    B_evenRows_evenColumns = np.delete(B_evenRows, range(0, B_evenRows.shape[1], 2), axis=1)
    B_filtered = B_evenRows_evenColumns

    return R, R_filtered, G, G1_filtered, G2_filtered, B, B_filtered  

def boxcarGenerator(fromWL, toWL, focus, width, height):
    """
    Generates a boxcar function with the given parameters, to be used for NIR filtering.
    Everything in nm, except the unitless "height".
    """
    
    # Checks
    if fromWL >= toWL:
        print("Incorrect wavelength limits!")

    if fromWL > focus or focus > toWL:
        print("Focused wavelength is outside limits!")
    

    # Prep
    step = 5 # nm
    samples = (toWL - fromWL) // step
    wls = np.linspace(fromWL, toWL, samples)
    heights = np.zeros( (samples,) )
    
    # Creating the pandas dataframe, empty for now.
    boxcar = pd.DataFrame({"wavelength":wls, "height":heights})

    # Populating the dataframe with the boxcar function.
    # The mask isolates the values inside the desire width.
    # I think pandas includes the last value, no need of >=, but make sure of that.
    mask = (boxcar["wavelength"] > focus - width / 2) & \
        (boxcar["wavelength"] < focus + width / 2)

    boxcar.update(boxcar.loc[mask, "height"] + height)

    return boxcar

# Unused!! It's the Bayer pattern
def filterAndMerge(image, RGBThroughput):
    """
    Filters an image according to its R, G and B throughput values.
    Image: read with cv2.imread.
    RGBThroughput: tuple with (R, G, B) values
    """

    # Splitting into channels (CV2 uses BGR)
    (B, G, R) = cv2.split(img)

    # Channel "degradation"
    np.multiply(R, RGBThroughput[0], out=R, casting="unsafe")
    np.multiply(G, RGBThroughput[1], out=G, casting="unsafe")
    np.multiply(B, RGBThroughput[2], out=B, casting="unsafe")

    # Bayer pattern
    # R G
    # G B

    # Red:
    # Checkered pattern
    R[::2, 1::2] = 0    
    # Removing even rows
    R[1::2] = 0

    # Green:
    # Two checkered patterns overlapped?

    G[::2, ::2] = 0
    G[1::2, 1::2] = 0

    # Blue
    # Checkered pattern, opposite to R's
    B[1::2, ::2] = 0
    # Removing odd rows
    B[::2] = 0

    # Now I'm getting a Blue image when saving red, Is the merge in BGR too? I thought it wasn't.

    # Remerging of channels
    #return cv2.merge([R, G, B]) # Yes, it seems that CV2 merge works with RGB. Weird.
    return cv2.merge([B, G, R])
