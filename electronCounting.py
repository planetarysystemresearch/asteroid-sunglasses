import matplotlib.pyplot as plt
import numpy as np
import cv2

# Variables

calibrationImage        =   "frame-calibration-1.png"
imageToBeCalibrated     =   "albedo1.png"

distanceToSun           =   1.0     # AU

## Camera parameters from MIRMIS
cameraAperture          =   13.6167     # mm
FoVx                    =   6.68        # degrees
FoVy                    =   5.36        # degrees
detectorX               =   640         # pixels
detectorY               =   512         # pixels

wavelength              =   800         # nm

materialAlbedo          =   0.05
optTrans                =   0.9
specTrans               =   0.3
specWindowWidth         =   25          # nm?

minLambda               =   700         # nm
maxLambda               =   1800        # nm


## CALIBRATION DISK

# Note to self:
# Radiosity is flux reflected.
# Irradiance is flux received.

def calibrationDiskRadiosity(calibrationImage, distanceToSun):
    """
    calibrationImage: string containing its relative path.
    distanceToSun:  int, in AU.
    """

    # Reading the image and the brightness of its center pixel.
    img = cv2.imread(calibrationImage)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    height, width = img.shape

    cx = int(width / 2)     # int rounds the numbers.
    cy = int(height / 2)
    center_px =  img[cx,cy]

    # Solar irradiance

    solarConstant = 1360    # W*m⁻² 

    solarIrradiance = solarConstant / (distanceToSun ** 2) # Wm⁻² ??? (distance in AU)

    # Lambertian radiosity 

    lambertianRadiosity = solarIrradiance / (2 * np.pi) # Wm⁻²sr⁻¹

    return(lambertianRadiosity)

def cameraPixelArea(cameraAperture, FoVx, FoVy, detectorX, detectorY):        # Is this a good name?
    """
    cameraAperture in mm.
    """

    targetDistance = 1  # Will cancel out

    # Combining everything into one formula will allow me to forget about the targetDistance
    
    cameraAperture = cameraAperture / 1000  # mm to m
    cameraApertureAngle = (np.pi * (cameraAperture / 2) ** 2) / (targetDistance ** 2) # steradians


    cameraAreaAtTarget = (2 * np.arctan(np.radians(FoVx) / 2) * targetDistance) * \
            (2 * np.arctan(np.radians(FoVy) / 2) * targetDistance)


    pixelAreaAtTarget = cameraAreaAtTarget / (detectorX * detectorY)

    return cameraApertureAngle * pixelAreaAtTarget

#calibrationFluxPerPixel = calibrationDiskRadiosity(calibrationImage, distanceToSun) * cameraPixelArea(cameraAperture, FoVx, FoVy, detectorX, detectorY)
#print(f"Flux per pixel in calibration image = {calibrationFluxPerPixel}  W")

def imageFlux(imageToBeCalibrated):
    """
    Reads image using cv2. Returns it as a numpy array of brightnesses
    """

    img = cv2.imread(imageToBeCalibrated)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    return(img/255)

#print(f"Max brightness value in original image: {np.amax(imageFlux(imageToBeCalibrated))}")

#fluxMatrix = imageFlux(imageToBeCalibrated) * calibrationFluxPerPixel  # W

######### TEST!
# Gonna fake the result to match Antti's calculations:

#fluxMatrix = np.full(np.shape(fluxMatrix), 5.72744E-10)

#############

#print(fluxMatrix)
#print(f"Max flux value: {np.amax(fluxMatrix)} W")


#-----------------------------------

# Function to find spectral flux for a given wavelength?
# Only for NIR in this example, must be done for other cameras?


def solarFlux():

    lambdas, fluxes = [], []

    with open("data/solar-input-energy-spectral-distribution.dat") as f:

        next(f)
        for line in (f):
            items = [float(x) for x in line.split('\t')]
            if minLambda <= items[0] <= maxLambda:
                lambdas.append(items[0])
                fluxes.append(items[1])
   
    return lambdas, fluxes

#xRange, yFlux = solarFlux()   # plot before choosing a wavelength


def spectralReflectance():
    """
    Reads and returns S-type asteroid spectra.
    """

    lambdas, spectra = [], []

    with open("data/S-type-asteroid-spectra.dat") as f:
        
        for line in f:
            
            items = [float(x) for x in line.split('\t')]
            if minLambda <= items[0]*1000 <= maxLambda: # lambda in micrometers in this file
                lambdas.append(items[0] * 1000)
                spectra.append(items[1])

        # Wavelengths to nm, the file contains micrometers.
        #lambdas = [i * 1000 for i in lambdas]

    return lambdas, spectra
    # What if you returned a numpy array directly?


#xSpectra, ySpectra = spectralReflectance()

# Have to interpolate xSpectra so that the number of values equals the other.

#interpolatedSpectra = np.interp(xRange, xSpectra, ySpectra) * materialAlbedo

#plt.plot(xRange, interpolatedSpectra)
#plt.show()


# And now multiply both to obtain Reflected spectral flux

#reflectedSpectralFlux = yFlux * interpolatedSpectra
# looks correct (units are wrong, but I still have to multiply stuff)
# Units are correct also when I overwrite all pixelFluxes with Antti's
        


# Optics transmission


#reflectedSpectraAfterOptTrans = reflectedSpectralFlux * optTrans

#plt.plot(xRange, reflectedSpectraAfterOptTrans)
#plt.show()



# Spectral transmission

specTrans = 0.3
specWindowWidth = 25

def moving_average(a, n) :

    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

# Multiplying by specWindowWidth to compensate for the average

#reflectedSpectraAfterOptSpecTrans = moving_average(specTrans * specWindowWidth * reflectedSpectraAfterOptTrans, specWindowWidth)

# Limiting xRange to the length of the averaged spectra
#xRange = xRange[12:-12]

#plt.plot(xRange, reflectedSpectraAfterOptSpecTrans)
#plt.show()

# Correct until here with "Antti's patch"

#------------------


def specificReflectedFlux(wavelength, xRange, reflectedSpectraAfterOptSpecTrans):

    return np.interp(wavelength, xRange, reflectedSpectraAfterOptSpecTrans)

#print(f"Reflected spectra at {wavelength} nm: {specificReflectedFlux(wavelength)} Wnm⁻¹")

# And multiply the flux matrix by this value!

#specificReflectedFluxMatrix = fluxMatrix * specificReflectedFlux(wavelength)
#print(f"Max specific reflected flux: {np.amax(specificReflectedFluxMatrix)} W")


#------------------

def photonEnergy(wavelength):

    planck  =   6.62606957E-34 # m²kgs⁻¹
    c       =   3E8 # ms⁻¹

    return (planck * c) / (wavelength * 1E-9) 

#print(f"Energy of photons with λ = {wavelength} nm: {photonEnergy(wavelength)} J")

# This should be reworked to work with matrices
#photonCount = specificReflectedFlux(wavelength) / photonEnergy(wavelength)
#print(f"Photon count: {photonCount} photons per second") # Wrong value, it seems
                                                        # And the photon energy is correct!
                                                        # Missing multiplying by the matrix!

#photonCountMatrix = specificReflectedFluxMatrix / photonEnergy(wavelength)
#print(f"Max photon count: {np.amax(photonCountMatrix)}")


def quantumEfficiency(wavelength):
    """
    The *1000 converts micrometers to nanometers.
    """

    quantumEfficiencies = np.loadtxt("data/NIR_quantum_efficiency.txt")
    #return quantumEfficiencies

    return np.interp(wavelength, quantumEfficiencies[:,0]*1000, quantumEfficiencies[:,1])

#QE = quantumEfficiency(wavelength)
#print(f"Quantum efficiency at {wavelength} nm: {QE}")


# And finally, I think, photonCount times something about QE gives electron count

#electronCountMatrix = photonCountMatrix * QE

#print(f"Max electron count: {np.amax(electronCountMatrix)} e⁻ per second")

# Export electron count matrix

#np.savetxt("resultElectronCountMatrix.txt", electronCountMatrix)


# Next: write one function to rule them all.

def imageToElectronCount(calibrationImage, imageToBeCalibrated, distanceToSun, wavelength, materialAlbedo, cameraAperture, FoVx, FoVy, detectorX, detectorY, optTrans, spectralTransmission, spectralWindowWidth):
    """
    Returns a numpy array with the electron counts for the given image, camera parameters, etc.
    calibrationDiscImage:   relative path of image.
    imageToBeCalibrated :   relative path as well.
    distanceToSun       :   in AU.
    wavelength          :   nm
    materialAlbedo      :   unitless
    cameraAperture      :   mm
    FoVx, FoVy          :   degrees
    detectorX, detectorY:   pixels
    """

    # Flux per detector pixel in the calibration image.
    calibrationFluxPerPixel = calibrationDiskRadiosity(calibrationImage, distanceToSun) * \
            cameraPixelArea(cameraAperture, FoVx, FoVy, detectorX, detectorY)

    # Flux per detector pixel in the image to be calibred.
    fluxMatrix = imageFlux(imageToBeCalibrated) * calibrationFluxPerPixel  # W

    # Solar flux data.
    xRange, yFlux = solarFlux()

    # The spectral reflectance data is sparser, needs to be interpolated.
    xSpectra, ySpectra = spectralReflectance()
    interpolatedSpectra = np.interp(xRange, xSpectra, ySpectra) * materialAlbedo

    reflectedSpectralFlux = yFlux * interpolatedSpectra

    # Optics transmission.
    reflectedSpectraAfterOptTrans = reflectedSpectralFlux * optTrans

    # Spectral transmission.
    # Note: multiplying by specWindowWidth to compensate for the effect of the average.

    reflectedSpectraAfterOptSpecTrans = moving_average(specTrans * specWindowWidth * \
            reflectedSpectraAfterOptTrans, specWindowWidth)

    # Limiting xRange to the length of the averaged spectra
    # Instead of 12 it should be half of spectralWindowWidth, rounded down?? Will this always work?
    xRange = xRange[12:-12]

    # Extract value for current wavelength and multiply by flux matrix.
    specificReflectedFluxMatrix = fluxMatrix * specificReflectedFlux(wavelength, \
            xRange, reflectedSpectraAfterOptSpecTrans)
    #specificReflectedFluxMatrix = fluxMatrix * specificReflectedFlux(wavelength)

    # Specific reflected flux matrix into photon count matrix.
    photonCountMatrix = specificReflectedFluxMatrix / photonEnergy(wavelength)

    # And finally, photonCount times QE gives electron count
    electronCountMatrix = photonCountMatrix * quantumEfficiency(wavelength)

    return electronCountMatrix

electronCountMatrix = imageToElectronCount(calibrationImage, imageToBeCalibrated, distanceToSun, wavelength, materialAlbedo, cameraAperture, FoVx, FoVy, detectorX, detectorY, optTrans, specTrans, specWindowWidth)
#print(np.amax(electronCountMatrix))
#print(np.amin(electronCountMatrix))

# After this function is finished, everything above it should be clean except for the functions
