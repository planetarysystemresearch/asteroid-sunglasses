import sys
import numpy as np
import pandas as pd

answer = input("This will overwrite vis_throughput.csv, if it's there. Continue? (Y/N)")

if answer.lower() != "y":
    sys.exit()

# Import VIS throughput table

vis = pd.read_csv("vis_throughput_original.csv")
# Column names are detected automatically, but row names are not.
# The first column is called "0" in the original file. Could use
# a better name, but I don't want to touch that.
vis = vis.set_index("0")

# Import Quantum efficiency table
quantum = pd.read_csv("quantum_eff_VIS.csv")
quantum.set_index("WL[nm]") # but quantum = quantum.set blabla gives an error??

# Trim quantum to the range of vis (350 - 900 nm)

# Try making these two lines into one command.
quantum = quantum[ quantum["WL[nm]"] >= 350 ]
quantum = quantum[ quantum["WL[nm]"] <= 900 ]
#print(quantum)

# Next up: extract one color from vis, same from quantum, divide.

# Need to keep one every three columns, skipping the first!
# Or the columns ending in nothing for red, .1 for green, and .2 for blue

red = vis.filter(regex = "\d{3,}$") # regex: three digits or more, then end of string.
print(red)

green = vis.filter(regex = "\d{3,}.1")  # three digits or more, followed by ".1"
#print(green)

blue = vis.filter(regex = "\d{3,}.2")  # three digits or more, followed by ".2"
#print(blue)

# Quantum:
quantum_red = quantum[["WL[nm]","QE_Red"]]
quantum_red = quantum_red.set_index("WL[nm]")
print(quantum_red)

quantum_green = quantum[["WL[nm]","QE_Green"]]
quantum_green = quantum_green.set_index("WL[nm]")

quantum_blue = quantum[["WL[nm]","QE_Blue"]]
quantum_blue = quantum_blue.set_index("WL[nm]")

"""
#empty dataframe, same size as red
division = pd.DataFrame().reindex_like(red).dropna()
"""

# Good, but do it with the size of vis!
division = pd.DataFrame().reindex_like(vis).dropna()
print(division)

"""
for index, row in red.iterrows():
    division = division.append(row / quantum_red.loc[index, "QE_Red"])

print(division)

division.to_csv("vis_throughput.csv")
"""

## Shit, I only did one color!!
# There has to be a way of alternating the columns later
# concat and then order? will that catch the 300 300.1 300.2?

red_corrected = pd.DataFrame().reindex_like(red).dropna()

for index, row in red.iterrows():
    red_corrected = red_corrected.append(row / quantum_red.loc[index, "QE_Red"])

#print(red_corrected)

green_corrected = pd.DataFrame().reindex_like(green).dropna()

for index, row in green.iterrows():
    green_corrected = green_corrected.append(row / quantum_green.loc[index, "QE_Green"])

#print(green_corrected)

blue_corrected = pd.DataFrame().reindex_like(blue).dropna()

for index, row in blue.iterrows():
    blue_corrected = blue_corrected.append(row / quantum_blue.loc[index, "QE_Blue"])

#print(blue_corrected)

result = pd.concat([red_corrected, green_corrected, blue_corrected], axis=1)
#and now order?
#result = result.reindex(sorted(result.columns), axis=1)
from natsort import natsorted

result = result.reindex(natsorted(result.columns), axis=1)

#wrong order because they are strings?
print(result)
result.to_csv("test.csv")
